package com.sotanna.groups.base;

import com.sotanna.groups.util.F;
import com.sotanna.groups.util.type.Gsonable;
import com.sotanna.groups.util.type.Reciever;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.UUID;

public class Member implements Gsonable<Member, UUID>, Reciever, Comparable<Member> {

    private final UUID id;

    private String Name;
    private UUID InGroup;

    public Member(UUID id) {
        this.id = id;
    }

    @Override
    public String name() {
        return Name;
    }

    public Member Name(String name) {
        Name = name;
        return this;
    }

    @Override
    public UUID id() {
        return id;
    }

    public UUID InGroup() {
        return InGroup;
    }

    public Member InGroup(UUID inGroup) {
        InGroup = inGroup;
        return this;
    }

    public Player player() {
        return Bukkit.getPlayer(id());
    }

    public boolean online() {
        Player player = player();
        return player != null && player.isOnline();
    }

    @Override
    public void broadcast(String message) {
        if (online()) player().sendMessage(F.format(message));
    }

    @Override
    public Class<? extends Member> type() {
        return Member.class;
    }

    @Override
    public int compareTo(Member o) {
        return o.name().compareTo(name());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Member member = (Member) o;

        return new EqualsBuilder()
                .append(id, member.id)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id)
                .toHashCode();
    }
}
