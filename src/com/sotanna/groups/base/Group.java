package com.sotanna.groups.base;

import com.sotanna.groups.Groups;
import com.sotanna.groups.event.GroupNameChangeEvent;
import com.sotanna.groups.event.member.MemberChangeRankEvent;
import com.sotanna.groups.event.member.MemberJoinGroupEvent;
import com.sotanna.groups.event.member.MemberLeaveGroupEvent;
import com.sotanna.groups.store.cache.GroupEntry;
import com.sotanna.groups.util.Messages;
import com.sotanna.groups.util.type.Gsonable;
import com.sotanna.groups.util.type.Reciever;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

public class Group implements Gsonable<Group, UUID>, Reciever {

    private GroupEntry Entry;
    private Map<UUID, Rank> Members = new HashMap<>();

    public Group(String name, Member leader) {
        Entry = new GroupEntry(name, UUID.randomUUID(), leader.name());
        Leader(leader);
        Name(name);
    }

    public GroupEntry Entry() {
        return Entry;
    }

    public Group Entry(GroupEntry entry) {
        Entry = entry;
        return this;
    }

    @Override
    public String name() {
        return Entry.name();
    }

    public Group Name(String name) {
        GroupNameChangeEvent event = Groups.call(new GroupNameChangeEvent(this, name(), name));
        if (event.isCancelled()) return this;

        Entry.Name(name);
        return this;
    }

    @Override
    public UUID id() {
        return Entry.id();
    }

    public Map<UUID, Rank> Members() {
        return Members;
    }

    public boolean online() {
        for (Member member : members()) if (member.online()) return true;
        return false;
    }

    public Member leader() {
        List<Member> leaders = Members(Rank.Leader);
        return leaders.isEmpty() ? null : leaders.get(0);
    }

    public Group Leader(Member member) {
        Member leader = leader();
        if (leader != null) Rank(leader(), false);

        Rank(member.InGroup(id()), Rank.Leader);
        Entry.Owner(member.name());
        return this;
    }

    public List<Member> members() {
        return Members().keySet().stream().map(uuid -> Groups.MemberStore().load(uuid)).collect(Collectors.toList());
    }

    public List<Member> Members(Rank rank) {
        return members().stream().filter(member -> Members().get(member.id()) == rank).collect(Collectors.toList());
    }

    public Rank rank(Member member) {
        Rank rank = Members().get(member.id());
        return rank == null ? Rank.Outsider : rank;
    }

    public Rank rank(Player player) {
        return rank(Groups.MemberStore().of(player));
    }

    public void Rank(Member member, Rank rank) {
        Members().put(member.id(), rank);
    }

    public boolean join(Member member) {
        Rank old = rank(member);
        if (old != Rank.Outsider) return false;

        MemberJoinGroupEvent event = Groups.call(new MemberJoinGroupEvent(this, member));
        if (event.isCancelled()) return false;

        Members().put(member.InGroup(id()).id(), Rank.Recruit);
        return true;
    }

    /**
     * Leave the group
     *
     * @param member The Member
     * @return True if they left, false if they can't, null if the group was deleted as a result
     */
    public Boolean leave(Member member) {
        Rank old = rank(member);
        if (old == Rank.Outsider) return false;

        MemberLeaveGroupEvent event = Groups.call(new MemberLeaveGroupEvent(this, member));
        if (event.isCancelled()) return false;

        boolean wasLeader = member.equals(leader());

        Members().remove(member.id());
        member.InGroup(null);

        if (Members().size() == 0) {
            Groups.GroupStore().delete(this);
            return null;
        }

        if (wasLeader) {
            List<Member> next = null;

            for (int l = Rank.values().length - 1; l >= 0; l--) {
                if (l == 0) break;

                next = Members(Rank.values()[l]);
                if (next.isEmpty()) continue;

                Leader(next.get(0));
                break;
            }

            if (next != null) next.clear();
            broadcast(Messages.Prefix + "&cYour leader has left and was replaced by &e" + leader().name());
        }

        return true;
    }

    public Boolean Rank(Member member, boolean up) {
        Rank current = rank(member);
        if (current == Rank.Outsider) return null;

        Rank next = up ? current.next() : current.last() == Rank.Outsider ? null : current.last();
        if (next == null) return false;

        MemberChangeRankEvent event = Groups.call(new MemberChangeRankEvent(this, member, current, next));
        if (event.isCancelled()) return false;

        Rank(member, next);
        return true;
    }

    @Override
    public void broadcast(String message) {
        members().forEach(member -> member.broadcast(message));
    }

    @Override
    public Class<? extends Group> type() {
        return Group.class;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Group group = (Group) o;

        return new EqualsBuilder()
                .append(id(), group.id())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(id())
                .toHashCode();
    }
}
