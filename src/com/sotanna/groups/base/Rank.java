package com.sotanna.groups.base;

import org.bukkit.ChatColor;

public enum Rank {

    Outsider (ChatColor.WHITE),

    Recruit  (ChatColor.GRAY),
    Member   (ChatColor.GREEN),
    Admin    (ChatColor.GOLD),
    Leader   (ChatColor.RED);

    private final ChatColor color;

    Rank(ChatColor color) {
        this.color = color;
    }

    public ChatColor color() {
        return color;
    }

    public boolean canInvite() {
        return this.ordinal() > Member.ordinal();
    }

    public Rank last() {
        return ordinal() > 0 ? values()[ordinal() - 1] : null;
    }

    public Rank next() {
        return ordinal() == values().length - 1 ? null : values()[ordinal() + 1];
    }

    public boolean higher(Rank other) {
        return other.ordinal() <= this.ordinal();
    }
}
