package com.sotanna.groups.gui;

import com.sotanna.groups.Groups;
import com.sotanna.groups.base.Group;
import com.sotanna.groups.base.Member;
import com.sotanna.groups.gui.base.ClickHandler;
import com.sotanna.groups.gui.base.Gui;
import com.sotanna.groups.invite.base.GroupInvite;
import com.sotanna.groups.util.Item;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.List;

public class GroupInvitesGui extends Gui {

    public GroupInvitesGui(Player player) {
        super(player, 45, "&3&lMy Invites");
    }

    @Override
    public void onBuild(Member member, Group group) {
        List<GroupInvite> invites = Groups.Invites().of(member);

        int on = 0, slot = 0;

        while (slot < 36 && on != invites.size()) {
            if (disabled(slot)) slot++;
            else set(invite(invites.get(on++)), ClickHandler.DoNothing(slot++));
        }
    }

    private ItemStack invite(GroupInvite invite) {
        return new Item(Material.LEASH).Name("&bInvite from &e" + invite.SentBy().name() + "&b to join &e" + invite.FromGroup().name()).Lore(" ", "&7(Left click to accept | Right click to deny)").get();
    }
}
