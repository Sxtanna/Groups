package com.sotanna.groups.gui.base;

import com.sotanna.groups.util.task.Call;

public abstract class ClickHandler implements Call<Click> {

    private Gui gui;
    private final int slot;

    public ClickHandler(int slot) {
        this.slot = slot;
    }

    public Gui gui() {
        return gui;
    }

    protected ClickHandler Gui(Gui gui) {
        this.gui = gui;
        return this;
    }

    public int slot() {
        return slot;
    }


    public static ClickHandler DoNothing(int slot) {
        return new ClickHandler(slot) {
            @Override
            public void call(Click data) {}
        };
    }
}
