package com.sotanna.groups.gui.base;


import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

public class Click {

    private Player player;
    private Gui gui;
    private ClickType clickType;
    private ItemStack itemStack;
    private int slot;

    public Click(Player player, Gui gui, ClickType clickType, ItemStack itemStack, int slot) {
        this.player = player;
        this.gui = gui;
        this.clickType = clickType;
        this.itemStack = itemStack;
        this.slot = slot;
    }

    public Player player() {
        return player;
    }

    public Gui gui() {
        return gui;
    }

    public ClickType clickType() {
        return clickType;
    }

    public ItemStack itemStack() {
        return itemStack;
    }

    public int slot() {
        return slot;
    }
}
