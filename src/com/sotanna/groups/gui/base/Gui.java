package com.sotanna.groups.gui.base;

import com.sotanna.groups.Groups;
import com.sotanna.groups.base.Group;
import com.sotanna.groups.base.Member;
import com.sotanna.groups.util.F;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.IntStream;

public abstract class Gui implements Listener {

    protected static final int[] Disabled = new int[]{0, 8, 9, 17, 18, 26, 27, 35};


    private final Player Player;
    private final Map<Integer, ClickHandler> ClickHandlers = new HashMap<>();

    private String title;
    private int size;
    private Inventory inventory;

    public Gui(Player player, int size, String title) {
        this.Player = player;
        this.size = size;
        this.title = F.format(title);
    }

    public String title() {
        return title;
    }

    public int size() {
        return size;
    }

    public Player player() {
        return Player;
    }

    public void open() {
        if (inventory == null) this.inventory = Bukkit.createInventory(null, size(), title());
        else this.inventory.clear();

        Member member = Groups.MemberStore().of(player());
        Group group = Groups.GroupStore().of(member);

        onBuild(member, group);

        Groups.Instance().getServer().getPluginManager().registerEvents(this, Groups.Instance());
        player().openInventory(this.inventory);
    }

    public void close() {
        player().closeInventory();
    }

    public abstract void onBuild(Member member, Group group);

    public void set(ItemStack itemStack, ClickHandler handler) {
        this.inventory.setItem(handler.slot(), itemStack);
        ClickHandlers.put(handler.slot(), handler.Gui(this));
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onClick(InventoryClickEvent e) {
        if (!e.getWhoClicked().equals(Player) || !e.getInventory().equals(this.inventory) || e.getCurrentItem() == null) return;
        e.setCancelled(true);

        ClickHandler handler = ClickHandlers.get(e.getSlot());
        if (handler != null) {
            Click click = new Click(((Player) e.getWhoClicked()), this, e.getClick(), e.getCurrentItem(), e.getSlot());
            handler.call(click);
        }
        else playSound(Sound.UI_BUTTON_CLICK, 5, 1);
    }

    @EventHandler
    public void onClose(InventoryCloseEvent e) {
        if (inventory == null || !e.getInventory().equals(this.inventory)) return;

        HandlerList.unregisterAll(this);
    }

    public void playSound(Sound sound, double volume, double pitch) {
        player().playSound(player().getLocation(), sound, ((float) volume), ((float) pitch));
    }

    protected boolean disabled(int slot) {
        return IntStream.of(Disabled).anyMatch(s -> s == slot);
    }
}
