package com.sotanna.groups.gui.base;

import com.sotanna.groups.store.cache.GroupEntry;
import com.sotanna.groups.util.Item;

import org.bukkit.Material;

public class GroupItem extends Item {

    public GroupItem(GroupEntry entry) {
        super(Material.CHEST);

        Name("&a&l" + entry.name());
        Lore(" ", "  &8&lLeader &f" + entry.owner(), "  &8&lID &f" + entry.id().toString(), " ", "&7Created on &e" + entry.created());
    }
}
