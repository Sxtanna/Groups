package com.sotanna.groups.gui;

import com.sotanna.groups.Groups;
import com.sotanna.groups.base.Group;
import com.sotanna.groups.base.Member;
import com.sotanna.groups.gui.base.ClickHandler;
import com.sotanna.groups.gui.base.GroupItem;
import com.sotanna.groups.gui.base.Gui;
import com.sotanna.groups.store.cache.GroupEntry;

import org.bukkit.entity.Player;

import java.util.List;

public class GroupsListGui extends Gui {

    private int page = 0;

    public GroupsListGui(Player player) {
        super(player, 45, "&6&lGroups List");
    }

    @Override
    public void onBuild(Member member, Group group) {
        List<GroupEntry> entries = Groups.GroupsCache().Entries();

        int on = (page * 28), slot = 0;

        while (slot < 36 && on != entries.size()) {
            if (disabled(slot)) slot++;
            else set(new GroupItem(entries.get(on++)).get(), ClickHandler.DoNothing(slot++));
        }
    }
}
