package com.sotanna.groups.gui;

import com.sotanna.groups.base.Group;
import com.sotanna.groups.base.Member;
import com.sotanna.groups.base.Rank;
import com.sotanna.groups.gui.base.Click;
import com.sotanna.groups.gui.base.ClickHandler;
import com.sotanna.groups.gui.base.Gui;
import com.sotanna.groups.util.Item;
import com.sotanna.groups.util.Messages;

import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class GroupGui extends Gui {

    private static final Item NameItem = new Item(Material.NAME_TAG);

    public GroupGui(Player player) {
        super(player, 27, "&e&lMy Group");
    }

    @Override
    public void onBuild(Member member, Group group) {
        set(nameTag(group), new ClickHandler(4) {
            @Override
            public void call(Click data) {

                if (group == null) {
                    data.gui().close();
                    member.broadcast(Messages.Prefix + (data.player().hasPermission("groups.create") ? "&7Type &f/group create <name>" : "&cYou don't have permission to create groups"));
                }
                else playSound(Sound.BLOCK_NOTE_PLING, 5, 1);
            }
        });
    }

    private ItemStack nameTag(Group group) {
        List<String> Members = new ArrayList<>();

        if (group != null) {
            for (int i = Rank.values().length - 1; i >= 0; i--) {
                if (i == 0) break;
                Rank rank = Rank.values()[i];

                List<Member> members = group.Members(rank);
                if (members.isEmpty()) continue;

                Members.add("                        ");
                Members.add(rank.color() + rank.name());

                Collections.sort(members, (o1, o2) -> o2.name().compareTo(o1.name()));
                if (members.size() > 5) members = members.subList(0, 5);

                members.forEach(mem -> Members.add("  &f" + mem.name()));
            }
        }
        return NameItem.copy().Name(group == null ? "&cYou aren't in a group" : "   &a" + group.name()).Lore(lore(Members)).get();
    }

    private String[] lore(List<String> lore) {
        return lore.toArray(new String[lore.size()]);
    }

}
