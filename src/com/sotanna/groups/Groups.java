package com.sotanna.groups;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import com.sotanna.groups.base.Group;
import com.sotanna.groups.base.Member;
import com.sotanna.groups.chat.GroupChat;
import com.sotanna.groups.command.GroupChatCommand;
import com.sotanna.groups.command.GroupCommand;
import com.sotanna.groups.invite.InviteHandler;
import com.sotanna.groups.store.GroupStore;
import com.sotanna.groups.store.MemberStore;
import com.sotanna.groups.store.cache.GroupsCache;
import com.sotanna.groups.util.Messages;

import org.bukkit.Bukkit;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;

public class Groups extends JavaPlugin implements Listener {

    public static final Gson Gson = new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create();

    private boolean enabled = false;

    private static Groups Instance;

    private GroupsCache GroupsCache;

    private GroupStore  GroupStore;
    private MemberStore MemberStore;

    private final InviteHandler Invites   = new InviteHandler();
    private final GroupChat     GroupChat = new GroupChat();

    private final GroupChatCommand GCCommand = new GroupChatCommand();
    private final GroupCommand     GCommand  = new GroupCommand();

    @Override
    public void onEnable() {
        Instance = this;
        saveDefaultConfig();

        if (!enabled) {
            GroupsCache = new GroupsCache(new File(getDataFolder(), "GroupsCache.json"));
            MemberStore = new MemberStore(new File(getDataFolder(), "Members"));
            GroupStore  = new GroupStore(new File(getDataFolder() , "Groups"), MemberStore);


            getServer().getPluginManager().registerEvents(GroupChat, Instance);
            getServer().getPluginManager().registerEvents(GroupsCache, Instance);
            getServer().getPluginManager().registerEvents(this, Instance);


            getCommand("GroupChat").setExecutor(GCCommand);
            getCommand("Group").setExecutor(GCommand);
            getCommand("Group").setTabCompleter(GCommand);

            Invites().startPoll();

            enabled = true;
        }
    }

    @Override
    public void onDisable() {
        GroupChat.clear();
        GroupsCache.save();

        MemberStore().values().forEach(member -> MemberStore().unload(member, true));
        GroupStore().values().forEach(group -> GroupStore().unload(group, true));
    }

    public void reload() {
        getLogger().info("Reloading Configuration");

        onDisable();
        Messages.clear();
        super.reloadConfig();
        onEnable();

        getLogger().info("Configuration Reloaded");
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        Member member = MemberStore.of(e.getPlayer());

        if (member.InGroup() != null) GroupStore.load(member.InGroup());
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
        if (!Instance.isEnabled()) return;

        Member member = MemberStore().load(e.getPlayer().getUniqueId());

        if (member.InGroup() == null) MemberStore().unload(member, true);
        else {
            if (GroupStore().loaded(member.InGroup())) {
                Bukkit.getScheduler().runTaskLater(Instance, () ->{
                    Group group = GroupStore().load(member.InGroup());
                    if (!group.online()) GroupStore().unload(group, true);

                }, 20L);
            }
        }
    }

    public static Groups Instance() {
        return Instance;
    }

    public static GroupStore GroupStore() {
        return Instance().GroupStore;
    }

    public static MemberStore MemberStore() {
        return Instance().MemberStore;
    }

    public static GroupsCache GroupsCache() {
        return Instance().GroupsCache;
    }

    public static InviteHandler Invites() {
        return Instance().Invites;
    }

    public static GroupChat GroupChat() {
        return Instance().GroupChat;
    }

    public static <E extends Event> E call(E event) {
        Instance.getServer().getPluginManager().callEvent(event);
        return event;
    }
}
