package com.sotanna.groups.chat;

import com.sotanna.groups.Groups;
import com.sotanna.groups.base.Group;
import com.sotanna.groups.base.Member;
import com.sotanna.groups.util.Messages;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import java.util.HashSet;
import java.util.UUID;

public class GroupChat implements Listener {

    /**
     * %playerName%
     * %groupName%
     * %rankColor%
     * %groupRank%
     * %message%
     */

    private final HashSet<UUID> InGroupChat = new HashSet<>();

    public boolean inGroupChat(Player player) {
        return InGroupChat.contains(player.getUniqueId());
    }

    public void toggle(Player player) {
        if (inGroupChat(player)) InGroupChat.remove(player.getUniqueId());
        else InGroupChat.add(player.getUniqueId());
    }

    public void clear() {
        InGroupChat.clear();
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onChat(AsyncPlayerChatEvent e) {
        if (e.isCancelled() || !inGroupChat(e.getPlayer())) return;

        Member member = member(e.getPlayer());
        if (member == null) return;

        Group group = group(member);

        String message = Messages.ChatFormat.toString()
                .replace("%prefix%"     , Messages.Prefix.toString())
                .replace("%playerName%" , e.getPlayer().getDisplayName())
                .replace("%groupName%"  , group.name())
                .replace("%rankColor%"  , group.rank(member).color().toString())
                .replace("%groupRank%"  , group.rank(member).name())
                .replace("%message%"    , e.getMessage());

        e.setCancelled(true);
        group.broadcast(message);
        Groups.Instance().getServer().getConsoleSender().sendMessage(member.name() + " > " + group.name() + " " + e.getMessage());
    }

    private Group group(Member member) {
        return Groups.GroupStore().load(member.InGroup());
    }

    private Member member(Player player) {
        return Groups.MemberStore().load(player.getUniqueId());
    }
}
