package com.sotanna.groups.invite.base;

import com.sotanna.groups.base.Group;
import com.sotanna.groups.base.Member;
import com.sotanna.groups.util.type.Gsonable;

public class GroupInvite implements Gsonable<GroupInvite, Long> {

    private final long ID, TimeStamp;
    private final Group FromGroup;
    private final Member SentBy, For;

    public GroupInvite(long id, Group fromGroup, Member sentBy, Member invFor) {
        ID = id;
        TimeStamp = System.currentTimeMillis();
        FromGroup = fromGroup;
        SentBy = sentBy;
        For = invFor;
    }

    public Group FromGroup() {
        return FromGroup;
    }

    public Member SentBy() {
        return SentBy;
    }

    public Member For() {
        return For;
    }

    public boolean timedOut() {
        return System.currentTimeMillis() - TimeStamp >= (60 * 1000);
    }

    @Override
    public Long id() {
        return ID;
    }

    @Override
    public String name() {
        return SentBy().name() + "<>" + FromGroup().name();
    }

    @Override
    public Class<? extends GroupInvite> type() {
        return GroupInvite.class;
    }
}
