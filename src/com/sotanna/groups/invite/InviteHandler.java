package com.sotanna.groups.invite;

import com.sotanna.groups.Groups;
import com.sotanna.groups.base.Group;
import com.sotanna.groups.base.Member;
import com.sotanna.groups.invite.base.GroupInvite;
import com.sotanna.groups.store.base.Store;
import com.sotanna.groups.util.Messages;

import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.List;
import java.util.stream.Collectors;

public class InviteHandler extends Store<Long, GroupInvite> {

    private static long lastID = 0L;

    public InviteHandler() {
        super(null, GroupInvite.class);
    }

    /**
     * Create a new GroupInvite
     *
     * @param id Unused as its chosen from Above
     * @param args 0 = The Group its from, 1 = The Member its from, 2 = The Member its for
     * @return The new Invite
     */
    @Override
    public GroupInvite create(Long id, Object... args) {
        if (!validate(args)) return null;

        return new GroupInvite(lastID++, ((Group) args[0]), ((Member) args[1]), ((Member) args[2]));
    }

    @Override
    public boolean validate(Object... args) {
        return args.length == 3 && args[0] instanceof Group && args[1] instanceof Member && args[2] instanceof Member;
    }

    @Override
    public GroupInvite store(GroupInvite val, boolean cache, boolean file) {
        return super.store(val, cache, false);
    }

    public List<GroupInvite> of(Member member) {
        return Cache().values().stream().filter(invite -> invite.For().equals(member)).collect(Collectors.toList());
    }

    public List<GroupInvite> of(Player player) {
        return of(Groups.MemberStore().of(player));
    }

    public boolean has(Member member, Group from) {
        return of(member).stream().filter(invite -> invite.FromGroup().equals(from)).findFirst().isPresent();
    }

    public boolean more(Member member) {
        return of(member).size() < 28;
    }

    public void handle(GroupInvite invite, boolean response) {
        if (response) {
            of(invite.For()).forEach(this::delete);

            invite.FromGroup().join(invite.For());
            invite.FromGroup().broadcast(Messages.Prefix + "&e" + invite.For().name() + "&b joined the group");
        }
        else {
            delete(invite);

            invite.For().broadcast(Messages.Prefix + "&cYou denied the invitation to &e" + invite.FromGroup().name());
            invite.FromGroup().broadcast(Messages.Prefix + "&e" + invite.For().name() + "&c rejected the invitation to join");
        }
    }

    public void startPoll() {
        new BukkitRunnable() {
            @Override
            public void run() {
                Cache().removeIf((invite) -> {
                    boolean timedOut = invite.timedOut();
                    if (timedOut) invite.For().broadcast(Messages.Prefix + "&cYour invite from &e" + invite.FromGroup().name() + "&c has timed out");

                    return timedOut;
                });
            }
        }.runTaskTimerAsynchronously(Groups.Instance(), 0, 20L);
    }
}
