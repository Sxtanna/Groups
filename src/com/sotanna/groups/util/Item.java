package com.sotanna.groups.util;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Item {
    
    private ItemStack itemStack;
    private ItemMeta itemMeta;

    public Item(ItemStack itemStack, ItemMeta itemMeta) {
        this.itemStack = itemStack;
        this.itemMeta = itemMeta;
    }

    public Item(ItemStack itemStack) {
        this(itemStack, itemStack.getItemMeta());
    }

    public Item(Material material, int data) {
        this(new ItemStack(material, 1, ((short) data)));
    }

    public Item(Material material) {
        this(material, 0);
    }

    public Item(Block block) {
        this(block.getType(), block.getData());
    }

    private ItemStack itemStack() {
        return itemStack;
    }

    protected ItemMeta itemMeta() {
        return itemMeta;
    }

    public Item Amount(int amount) {
        itemStack().setAmount(amount);
        return this;
    }

    public Item Name(String name) {
        itemMeta().setDisplayName(F.format(name));
        return this;
    }

    public Item Lore(String... lores) {
        if (lores.length > 0) itemMeta().setLore(F.format(lores));
        else itemMeta().setLore(null);
        return this;
    }

    public Item Enchant(Enchantment enchantment, int level) {
        itemMeta().addEnchant(enchantment, level, true);
        return this;
    }

    public Item Enchant(Enchantment enchantment) {
        return Enchant(enchantment, 1);
    }

    public Item Flag(ItemFlag... itemFlags) {
        itemMeta().addItemFlags(itemFlags);
        return this;
    }

    public Item Unbreakable(boolean state) {
        itemMeta().spigot().setUnbreakable(state);
        return this;
    }

    public Item Glow(boolean state) {
        if (state) {
            Enchant(Enchantment.OXYGEN);
            Flag(ItemFlag.HIDE_ENCHANTS);
        }
        else {
            itemMeta().removeEnchant(Enchantment.OXYGEN);
            itemMeta().removeItemFlags(ItemFlag.HIDE_ENCHANTS);
        }
        return this;
    }

    public ItemStack get() {
        try {
            itemStack().setItemMeta(itemMeta());
        }
        catch (Exception e) {}
        return itemStack();
    }

    public Item copy() {
        return new Item(get());
    }
}
