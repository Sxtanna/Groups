package com.sotanna.groups.util;

import com.sotanna.groups.Groups;
import com.sotanna.groups.base.Group;
import com.sotanna.groups.base.Member;
import com.sotanna.groups.util.task.MemberCall;

import org.bukkit.Bukkit;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class MatchMember {

    private static void match(List<Member> from, MemberCall possible) {
        String match = possible.namePart().toLowerCase();

        List<Member> matches = new ArrayList<>();

        for (Member option : from) {
            String name = option.name().toLowerCase();

            if (name.equals(match)) {
                matches.clear();
                matches.add(option);
                break;
            }

            if (name.startsWith(match) || name.endsWith(match) || (match.length() >= 3 && name.contains(match))) matches.add(option);
        }

        if (matches.size() == 1) possible.call(matches.get(0));
        else if (matches.size() == 0) possible.fail("Not Found");
        else possible.fail("&3&lMember Matcher: &a" + (matches.size() > 5 ? "That name matches &e" + matches.size() + " &aMembers" : F.combine(names(matches), "&7, &a")));
    }

    public static void matchOnline(MemberCall possible) {
        List<Member> online = new ArrayList<>();
        Bukkit.getOnlinePlayers().forEach(player -> online.add(Groups.MemberStore().of(player)));

        match(online, possible);
    }

    public static void matchInGroup(Group group, MemberCall possible) {
        match(group.members(), possible);
    }

    private static List<String> names(List<Member> members) {
        List<String> names = members.stream().map(Member::name).collect(Collectors.toList());
        Collections.sort(names);
        return names;
    }
}
