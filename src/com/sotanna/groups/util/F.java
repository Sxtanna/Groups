package com.sotanna.groups.util;

import org.bukkit.ChatColor;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class F {

    public static String format(String input) {
        return ChatColor.translateAlternateColorCodes('&', input);
    }

    public static List<String> format(String... strings) {
        return Stream.of(strings).map(F::format).collect(Collectors.toList());
    }

    public static String combine(List<String> strings, String... sep) {
        if (strings == null || strings.size() == 0) return "[]";

        return String.join(sep.length > 0 ? sep[0] : "", strings);
    }
}
