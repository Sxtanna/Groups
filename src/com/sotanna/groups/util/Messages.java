package com.sotanna.groups.util;

import com.sotanna.groups.Groups;

public enum Messages {

    ChatFormat ("&2[&a%groupName%&2] &6%groupRank% &f%playerName% &r%message%", "Format.GroupChat"),
    Prefix     ("&f&l[&2&lG&f&l] "                , "Messages.Prefix"),
    GCEnabled  ("%prefix%&7Group Chat &aEnabled"  , "Messages.GroupChat-Enable"),
    GCDisabled ("%prefix%&7Group Chat &cDisabled" , "Messages.GroupChat-Disable"),
    NotInGroup ("%prefix%&cYou aren't in a Group" , "Messages.Not-In-Group");

    private final String defaultMsg;
    private String configPath, configMsg;

    Messages(String defaultMsg, String configPath) {
        this.defaultMsg = defaultMsg;
        this.configPath = configPath;
    }

    @Override
    public String toString() {
        return message().replace("%prefix%", Prefix.message());
    }

    public String message() {
        if (configMsg == null) configMsg = Groups.Instance().getConfig().getString(configPath, defaultMsg);
        return configMsg;
    }

    public static void clear() {
        for (Messages messages : values()) messages.configMsg = null;
    }
}
