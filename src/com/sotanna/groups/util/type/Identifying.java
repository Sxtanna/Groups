package com.sotanna.groups.util.type;

public interface Identifying<I> {

    I id();

    String name();
}
