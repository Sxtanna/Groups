package com.sotanna.groups.util.type;

public interface Builder<B> {

    @SuppressWarnings("unchecked")
    default B thisClass() {
        return ((B) this);
    }

}
