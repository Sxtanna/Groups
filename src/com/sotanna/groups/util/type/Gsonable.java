package com.sotanna.groups.util.type;

import com.sotanna.groups.Groups;

@SuppressWarnings("unchecked")
public interface Gsonable<T extends Gsonable, I> extends Identifying<I> {

    Class<? extends T> type();

    default String to() {
        return Groups.Gson.toJson(this, type());
    }

    default T onFrom() {
        return ((T) this);
    }

    static<T extends Gsonable> T from(String string, Class<T> typeClass) {
        return (T) Groups.Gson.fromJson(string, typeClass).onFrom();
    }
}
