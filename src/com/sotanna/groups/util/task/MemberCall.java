package com.sotanna.groups.util.task;

import com.sotanna.groups.base.Member;
import com.sotanna.groups.util.F;
import com.sotanna.groups.util.Messages;

import org.bukkit.entity.Player;

public abstract class MemberCall implements Call<Member> {

    private Player player;
    private String namePart;

    public MemberCall(Player player, String namePart) {
        this.player = player;
        this.namePart = namePart;
    }

    public String namePart() {
        return namePart;
    }

    public void fail(String fail) {
        if (fail.equals("Not Found")) player.sendMessage(F.format(Messages.Prefix + "&cPlayer &e" + namePart + "&c not found"));
        else player.sendMessage(F.format(Messages.Prefix + fail));
    }
}
