package com.sotanna.groups.util.task;

public interface Call<D> {

    void call(D data);

}
