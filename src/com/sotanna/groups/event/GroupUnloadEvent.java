package com.sotanna.groups.event;

import com.sotanna.groups.base.Group;
import com.sotanna.groups.event.base.GroupEvent;

import org.bukkit.event.HandlerList;

public class GroupUnloadEvent extends GroupEvent {

    private static final HandlerList Handlers = new HandlerList();

    private final boolean deleted;

    public GroupUnloadEvent(Group group, boolean deleted) {
        super(group);
        this.deleted = deleted;
    }

    public boolean deleted() {
        return deleted;
    }

    /**
     * Cannot cancel the unloading of a group
     *
     * @param b Unused Cancelled state
     */
    @Override
    public void setCancelled(boolean b) {}

    @Override
    public HandlerList getHandlers() {
        return Handlers;
    }

    public static HandlerList getHandlerList() {
        return Handlers;
    }
}
