package com.sotanna.groups.event.member;

import com.sotanna.groups.base.Group;
import com.sotanna.groups.base.Member;
import com.sotanna.groups.base.Rank;
import com.sotanna.groups.event.base.GroupEvent;

import org.bukkit.event.HandlerList;

public class MemberChangeRankEvent extends GroupEvent {

    private static final HandlerList Handlers = new HandlerList();


    private final Member member;
    private Rank oldRank, newRank;

    public MemberChangeRankEvent(Group group, Member member, Rank oldRank, Rank newRank) {
        super(group);
        this.member = member;
        this.oldRank = oldRank;
        this.newRank = newRank;
    }

    public Member member() {
        return member;
    }

    public Rank oldRank() {
        return oldRank;
    }

    public Rank newRank() {
        return newRank;
    }

    public MemberChangeRankEvent NewRank(Rank newRank) {
        this.newRank = newRank;
        return this;
    }

    @Override
    public HandlerList getHandlers() {
        return Handlers;
    }

    public static HandlerList getHandlerList() {
        return Handlers;
    }
}
