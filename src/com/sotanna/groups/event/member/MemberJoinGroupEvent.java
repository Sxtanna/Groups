package com.sotanna.groups.event.member;

import com.sotanna.groups.base.Group;
import com.sotanna.groups.base.Member;
import com.sotanna.groups.event.base.GroupEvent;

import org.bukkit.event.HandlerList;

public class MemberJoinGroupEvent extends GroupEvent {

    private static final HandlerList Handlers = new HandlerList();


    private final Member member;

    public MemberJoinGroupEvent(Group group, Member member) {
        super(group);
        this.member = member;
    }

    public Member member() {
        return member;
    }

    @Override
    public HandlerList getHandlers() {
        return Handlers;
    }

    public static HandlerList getHandlerList() {
        return Handlers;
    }
}
