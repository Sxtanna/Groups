package com.sotanna.groups.event;

import com.sotanna.groups.base.Group;
import com.sotanna.groups.event.base.GroupEvent;

import org.bukkit.event.HandlerList;

public class GroupLoadEvent extends GroupEvent {

    private static final HandlerList Handlers = new HandlerList();


    private final boolean newGroup;

    public GroupLoadEvent(Group group, boolean newGroup) {
        super(group);
        this.newGroup = newGroup;
    }

    public boolean newGroup() {
        return newGroup;
    }

    /**
     * Cannot cancel the loading of a group
     *
     * @param b Unused Cancelled state
     */
    @Override
    public void setCancelled(boolean b) {}

    @Override
    public HandlerList getHandlers() {
        return Handlers;
    }

    public static HandlerList getHandlerList() {
        return Handlers;
    }

}
