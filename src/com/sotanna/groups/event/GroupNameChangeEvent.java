package com.sotanna.groups.event;

import com.sotanna.groups.base.Group;
import com.sotanna.groups.event.base.GroupEvent;

import org.bukkit.event.HandlerList;

public class GroupNameChangeEvent extends GroupEvent {

    private static final HandlerList Handlers = new HandlerList();

    private String oldName, newName;

    public GroupNameChangeEvent(Group group, String oldName, String newName) {
        super(group);
        this.oldName = oldName;
        this.newName = newName;
    }

    public String oldName() {
        return oldName;
    }

    public String newName() {
        return newName;
    }

    public GroupNameChangeEvent NewName(String newName) {
        this.newName = newName;
        return this;
    }

    @Override
    public HandlerList getHandlers() {
        return Handlers;
    }

    public static HandlerList getHandlerList() {
        return Handlers;
    }
}
