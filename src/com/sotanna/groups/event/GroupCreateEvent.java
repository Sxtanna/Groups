package com.sotanna.groups.event;

import com.sotanna.groups.base.Member;

import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class GroupCreateEvent extends Event implements Cancellable {

    private static final HandlerList Handlers = new HandlerList();

    private boolean cancelled = false;

    private String name;
    private Member leader;

    public GroupCreateEvent(String name, Member leader) {
        this.name = name;
        this.leader = leader;
    }

    public String name() {
        return name;
    }

    public GroupCreateEvent Name(String name) {
        this.name = name;
        return this;
    }

    public Member leader() {
        return leader;
    }

    public GroupCreateEvent Leader(Member leader) {
        this.leader = leader;
        return this;
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean b) {
        this.cancelled = b;
    }

    @Override
    public HandlerList getHandlers() {
        return Handlers;
    }

    public static HandlerList getHandlerList() {
        return Handlers;
    }
}
