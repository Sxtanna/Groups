package com.sotanna.groups.event.base;

import com.sotanna.groups.base.Group;

import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class GroupEvent extends Event implements Cancellable {

    private static final HandlerList Handlers = new HandlerList();


    private boolean cancelled = false;
    private Group group;

    public GroupEvent(Group group) {
        this.group = group;
    }

    public Group group() {
        return group;
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean b) {
        this.cancelled = b;
    }

    @Override
    public HandlerList getHandlers() {
        return Handlers;
    }

    public static HandlerList getHandlerList() {
        return Handlers;
    }
}
