package com.sotanna.groups.command;

import com.sotanna.groups.Groups;
import com.sotanna.groups.base.Group;
import com.sotanna.groups.util.F;
import com.sotanna.groups.util.Messages;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class GroupChatCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String alias, String[] args) {
        if (!(sender instanceof Player)) sender.sendMessage(F.format("&cThis command can only be ran in game"));
        else {
            Player player = ((Player) sender);

            Group group = Groups.GroupStore().of(player);
            if (group == null) player.sendMessage(F.format(String.valueOf(Messages.NotInGroup)));
            else{
                Groups.GroupChat().toggle(player);
                player.sendMessage(F.format(String.valueOf(Groups.GroupChat().inGroupChat(player) ? Messages.GCEnabled : Messages.GCDisabled)));
            }
        }
        return true;
    }

}
