package com.sotanna.groups.command;

import com.sotanna.groups.Groups;
import com.sotanna.groups.base.Group;
import com.sotanna.groups.base.Member;
import com.sotanna.groups.base.Rank;
import com.sotanna.groups.gui.GroupGui;
import com.sotanna.groups.gui.GroupInvitesGui;
import com.sotanna.groups.gui.GroupsListGui;
import com.sotanna.groups.invite.base.GroupInvite;
import com.sotanna.groups.util.MatchMember;
import com.sotanna.groups.util.Messages;
import com.sotanna.groups.util.task.MemberCall;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

import static com.sotanna.groups.util.F.format;
import static com.sotanna.groups.util.Messages.Prefix;

public class GroupCommand implements CommandExecutor, TabCompleter {

    private static final String[] Args = {"Reload", "Create", "Accept", "Deny", "Leave", "Invite", "Invites", "Kick", "List", "Promote", "Demote"};

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String alias, String[] args) {
        if (args.length == 0) {
            if (checkPlayer(sender)) new GroupGui(((Player) sender)).open();
        } else {
            Group group = !(sender instanceof Player) ? null : Groups.GroupStore().of(((Player) sender));
            Member member = !(sender instanceof Player) ? null : Groups.MemberStore().of(((Player) sender));

            switch (args[0].toLowerCase()) {
                case "reload":
                    if (checkPerm(sender, "groups.admin")) {
                        Groups.Instance().reload();
                        sendMessage(sender, "Reloaded!");
                    }
                    break;
                case "create":
                    runCreate(sender, group, args);
                    break;
                case "accept":
                case "deny":
                    boolean accepting = args[0].toLowerCase().equals("accept");
                    if (args.length < 2) sendMessage(sender, "&cWho's invite do you want to " + (accepting ? "Accept" : "Deny"));
                    else {
                        runInviteHandle(sender, group, member, args[1], accepting);
                    }
                    break;
                case "leave":
                    runLeave(sender, group);
                    break;
                case "invite":
                    runInvite(sender, group, member, args);
                    break;
                case "invites":
                    runInvites(sender);
                    break;
                case "kick":
                    runKick(sender, group, member, args);
                    break;
                case "list":
                    runList(sender);
                    break;
                case "promote":
                    runRankChange(sender, group, member, args, true);
                    break;
                case "demote":
                    runRankChange(sender, group, member, args, false);
                    break;
            }
        }
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String alias, String[] args) {
        List<String> parsed = new ArrayList<>();

        switch (args[0].toLowerCase()) {
            case "promote":
            case "demote":
            case "kick":
                if (!(sender instanceof Player)) sendMessage(sender, "&cYou can't kick anyone, you're not in a group");
                else {
                    if (args.length > 2) return parsed;

                    Group group = Groups.GroupStore().of(((Player) sender));
                    boolean leader = group.rank(((Player) sender)) == Rank.Leader;

                    group.members().forEach(member -> {
                        if ((!group.rank(member).canInvite() || leader) && !member.id().equals(((Player) sender).getUniqueId())) parsed.add(member.name());
                    });
                }

                if (parsed.isEmpty()) return parsed;
                break;
            case "accept":
                if (!(sender instanceof Player)) sendMessage(sender, "&cYou can't accept invites, you aren't a player");
                else {
                    if (args.length > 2) return parsed;

                    Group group = Groups.GroupStore().of(((Player) sender));
                    if (group != null) {
                        sendMessage(sender, "&cLeave your current group before trying to join another");
                        return parsed;
                    }
                    else Groups.Invites().of(((Player) sender)).forEach(invite -> parsed.add(invite.FromGroup().name()));
                }

                if (parsed.isEmpty()) return parsed;
                break;
            case "invite":
                return null;
        }

        if (parsed.isEmpty() && args.length < 2) {
            for (String arg : Args) {
                if (arg.toLowerCase().startsWith(args[0].toLowerCase())) parsed.add(arg);
            }
        }

        return parsed;
    }

    private void runRankChange(CommandSender sender, Group current, Member member, String[] args, boolean promote) {
        if (!checkPlayer(sender)) return;

        if (checkInGroup(((Player) sender), current, true)) {
            Rank rank = current.rank(member);

            if (!rank.canInvite()) sendMessage(sender, "&cYou can't " + (promote ? "promote" : "demote" + " members"));
            else {
                MatchMember.matchInGroup(current, new MemberCall(((Player) sender), args[1]) {
                    @Override
                    public void call(Member data) {
                        Rank other = current.rank(data);
                        boolean owner = current.leader().equals(member);

                        if (other.higher(rank) && !owner) sendMessage(sender, "&cYou can't promote or demote a rank higher than yours higher");
                        else {
                            Boolean changed = current.Rank(data, promote);
                            if (changed != null && changed) {
                                other = current.rank(data);

                                current.broadcast("&e" + data.name() + "&b has been " + (promote ? "promoted" : "demoted") + " to " + other.color() + other.name());
                            }
                            else sendMessage(sender, "&e" + data.name() + "&c cannot be " + (promote ? "promoted" : "demoted") + " further");
                        }
                    }
                });
            }
        }
    }

    private void runList(CommandSender sender) {
        if (checkPerm(sender, "groups.list", "&cYou don't have permission to list groups")) {
            if (!checkPlayer(sender)) return;

            new GroupsListGui(((Player) sender)).open();
        }
    }

    private void runCreate(CommandSender sender, Group current, String[] args) {
        if (checkPerm(sender, "groups.create", "&cYou don't have permission to create groups")) {
            if (!checkPlayer(sender)) return;

            if (checkInGroup(((Player) sender), current, false)) {
                if (args.length < 2) sendMessage(sender, "&cYou must provide a name for your group");
                else {
                    Groups.GroupsCache().exists(args[1], data -> {
                        if (data) sendMessage(sender, "&cA Group with that name already exists");
                        else {
                            Groups.GroupStore().loadOrCreate(null, args[1], Groups.MemberStore().of(((Player) sender)));
                            sendMessage(sender, "&7Created group &e" + args[1]);
                        }
                    });
                }
            }
        }
    }

    private void runKick(CommandSender sender, Group current, Member member, String[] args) {
        if (!checkPlayer(sender)) return;

        if (checkInGroup(((Player) sender), current, true)) {
            if (!current.rank(member).canInvite()) sendMessage(sender, "&cYou can't kick anyone");
            else {
                if (args.length < 2) sendMessage(sender, "&cWho do you want to kick?");
                else {
                    MatchMember.matchInGroup(current, new MemberCall(((Player) sender), args[1]) {
                        @Override
                        public void call(Member data) {
                            if (current.rank(data) == Rank.Leader) sendMessage(sender, "&cYou cannot kick the leader of a Group");
                            else {
                                current.leave(data);
                                current.broadcast(Messages.Prefix + "&e" + data.name() + "&c was kicked from the group by &e" + member.name());
                            }
                        }
                    });
                }
            }
        }
    }

    private void runLeave(CommandSender sender, Group current) {
        if (!checkPlayer(sender)) return;

        if (checkInGroup(((Player) sender), current, true)) {
            Boolean leave = current.leave(Groups.MemberStore().of(((Player) sender)));

            if (leave == null) {
                sendMessage(sender, "&c&lDeleted group &e" + current.name() + " &8&l[&fLack of Members&8&l]");
                leave = true;
            }

            sendMessage(sender, (leave ? "&7You left group &e" : "&cYou can't leave group &e") + current.name());
        }
    }

    private void runInvite(CommandSender sender, Group current, Member member, String[] args) {
        if (!checkPlayer(sender)) return;

        if (checkInGroup(((Player) sender), current, true)) {
            Rank rank = current.rank(Groups.MemberStore().of(((Player) sender)));

            if (!rank.canInvite()) sendMessage(sender, "&cYou aren't allowed to invite people");
            else if (args.length < 2) sendMessage(sender, "&cWho do you want to invite?");
            else {
                MatchMember.matchOnline(new MemberCall(((Player) sender), args[1]) {
                    @Override
                    public void call(Member data) {
                        if (data.equals(member))
                            sendMessage(sender, "&cYou cannot invite yourself");
                        else {
                            if (Groups.Invites().has(data, current))
                                sendMessage(sender, "&e" + data.name() + "&c has already been invited");
                            else {
                                Groups.Invites().loadOrCreate(null, current, member, data);

                                data.broadcast(Messages.Prefix + "&bYou have been invited to &e" + current.name() + "&b by &e" + member.name());
                                current.broadcast(Messages.Prefix + "&e" + data.name() + "&b was invited by &e" + member.name());
                            }
                        }
                    }
                });
            }
        }
    }

    private void runInviteHandle(CommandSender sender, Group current, Member member, String groupName, boolean response) {
        if (!checkPlayer(sender)) return;

        if (checkInGroup(((Player) sender), current, false) || !response) {
            GroupInvite target = null;

            for (GroupInvite invite : Groups.Invites().of(member)) {
                if (invite.FromGroup().name().equalsIgnoreCase(groupName)) {
                    target = invite;
                    break;
                }
            }

            if (target == null) sendMessage(sender, "&cYou don't have an invite from &e" + groupName);
            else Groups.Invites().handle(target, response);
        }
    }

    private void runInvites(CommandSender sender) {
        if (!checkPlayer(sender)) return;

        new GroupInvitesGui(((Player) sender)).open();
    }


    private boolean checkPerm(CommandSender sender, String permission, String... ifNot) {
        if (sender.hasPermission(permission)) return true;
        else {
            sendMessage(sender, (ifNot.length == 0 ? "&cYou don't have permission for this" : ifNot[0]));
            return false;
        }
    }

    private boolean checkPlayer(CommandSender sender) {
        if (sender instanceof Player) return true;
        else {
            sendMessage(sender, "&cThis command can only be ran in game");
            return false;
        }
    }

    private boolean checkInGroup(Player player, Group current, boolean shouldBe) {
        if (current == null && shouldBe) {
            sendMessage(player, "&cYou aren't in a group");
            return false;
        }
        else if (current != null && !shouldBe) {
            sendMessage(player, "&cYou are already in a group");
            return false;
        }
        return true;
    }


    private void sendMessage(CommandSender sender, String message) {
        sender.sendMessage(format(Prefix + message));
    }
}
