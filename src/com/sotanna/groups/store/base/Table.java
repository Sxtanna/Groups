package com.sotanna.groups.store.base;

import com.sotanna.groups.util.type.Identifying;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Table<I, T extends Identifying<I>> {

    private final Map<String, I> NameMap = new HashMap<>();
    private final Map<I, T>      IDMap = new HashMap<I, T>() {
        @Override
        public T put(I key, T value) {
            NameMap.put(value.name().toLowerCase(), key);
            return super.put(key, value);
        }

        @Override
        public T remove(Object key) {
            NameMap.remove(get(key).name().toLowerCase());
            return super.remove(key);
        }
    };


    void add(T val) {
        IDMap.put(val.id(), val);
    }

    void remove(T val) {
        IDMap.remove(val.id());
    }

    public boolean removeIf(Predicate<? super T> filter) {
        Objects.requireNonNull(filter);
        boolean removed = false;

        final Iterator<Map.Entry<I, T>> each = IDMap.entrySet().iterator();
        while (each.hasNext()) {
            Map.Entry<I, T> next = each.next();

            if (filter.test(next.getValue())) {
                each.remove();
                NameMap.remove(next.getValue().name().toLowerCase());
                removed = true;
            }
        }
        return removed;
    }

    public T byName(String name) {
        return byID(NameMap.get(name.toLowerCase()));
    }

    public T byID(I id) {
        return id == null ? null : IDMap.get(id);
    }

    public boolean contains(I id) {
        return id != null && IDMap.containsKey(id);
    }

    public boolean contains(String name) {
        return name != null && NameMap.containsKey(name.toLowerCase());
    }

    public boolean contains(T val) {
        return val != null && contains(val.id());
    }

    public void clear() {
        IDMap.clear();
        NameMap.clear();
    }

    public List<I> ids() {
        return IDMap.keySet().stream().collect(Collectors.toList());
    }

    public List<T> values() {
        return IDMap.values().stream().collect(Collectors.toList());
    }

}
