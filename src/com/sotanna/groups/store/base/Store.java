package com.sotanna.groups.store.base;

import com.sotanna.groups.util.type.Gsonable;
import com.sotanna.groups.util.JsonFile;

import java.io.File;
import java.util.List;

public abstract class Store<I, T extends Gsonable<T, I>> {

    private final File StorageFolder;
    private final Table<I, T> Cache = new Table<>();
    private final Class<T> tClass;

    public Store(File storageFolder, Class<T> tClass) {
        StorageFolder = storageFolder;
        this.tClass = tClass;
    }

    public File StorageFolder() {
        return StorageFolder;
    }

    public Table<I, T> Cache() {
        return Cache;
    }

    public T load(I id) {
        if (id == null) return null;

        if (loaded(id)) return Cache().byID(id);

        JsonFile file = new JsonFile(new File(StorageFolder(), id.toString() + ".json"));
        if (!file.exists()) return null;

        T val = file.load(tClass);
        Cache().add(val);

        return val;
    }

    public void delete(T val) {
        unload(val, false);

        File file = new File(StorageFolder(), val.id().toString() + ".json");
        if (file.exists()) file.delete();
    }

    public void unload(T val, boolean save) {
        if (save) store(val, false, true);
        Cache().remove(val);
    }

    public void unload(I id, boolean save) {
        if (!loaded(id)) return;

        T val = load(id);
        unload(val, save);
    }

    public T store(T val, boolean cache, boolean file) {
        if (cache) Cache().add(val);
        if (file) new JsonFile(new File(StorageFolder(), val.id().toString() + ".json")).save(val);

        return val;
    }

    public T loadOrCreate(I id, Object... args) {
        T val = load(id);
        if (val != null) return val;

        return store(create(id, args), true, true);
    }

    public List<T> values() {
        return Cache().values();
    }

    public abstract T create(I id, Object... args);

    public abstract boolean validate(Object... args);

    public boolean loaded(I id) {
        return Cache().contains(id);
    }
}
