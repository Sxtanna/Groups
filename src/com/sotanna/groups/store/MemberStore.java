package com.sotanna.groups.store;

import com.sotanna.groups.base.Member;
import com.sotanna.groups.store.base.Store;

import org.bukkit.entity.Player;

import java.io.File;
import java.util.UUID;

public class MemberStore extends Store<UUID, Member> {

    public MemberStore(File storageFolder) {
        super(storageFolder, Member.class);
    }

    /**
     * Create a new Member
     *
     * @param id The Player's UUID
     * @param args 0 = The Player's name
     * @return The new Member
     */
    @Override
    public Member create(UUID id, Object... args) {
        if (!validate(args)) return null;

        return new Member(id).Name(((String) args[0]));
    }

    @Override
    public boolean validate(Object... args) {
        return args.length == 1 && args[0] instanceof String;
    }

    public Member of(Player player) {
        return loadOrCreate(player.getUniqueId(), player.getName());
    }
}
