package com.sotanna.groups.store.cache;

import com.sotanna.groups.util.type.Gsonable;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.UUID;

public class GroupEntry implements Gsonable<GroupEntry, UUID> {

    private final UUID uuid;
    private final Long timestamp;
    private String name, owner;

    public GroupEntry(String name, UUID uuid, String owner) {
        this.name = name;
        this.uuid = uuid;
        this.owner = owner;

        this.timestamp = System.currentTimeMillis();
    }

    @Override
    public UUID id() {
        return uuid;
    }

    @Override
    public String name() {
        return name;
    }

    public String created() {
        return Timestamp.from(Instant.ofEpochMilli(timestamp)).toString();
    }

    public GroupEntry Name(String name) {
        this.name = name;
        return this;
    }

    public String owner() {
        return owner;
    }

    public GroupEntry Owner(String owner) {
        this.owner = owner;
        return this;
    }

    @Override
    public Class<? extends GroupEntry> type() {
        return GroupEntry.class;
    }
}
