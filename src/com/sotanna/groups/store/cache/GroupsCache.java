package com.sotanna.groups.store.cache;

import com.sotanna.groups.event.GroupLoadEvent;
import com.sotanna.groups.event.GroupUnloadEvent;
import com.sotanna.groups.util.JsonFile;
import com.sotanna.groups.util.task.Call;
import com.sotanna.groups.util.type.Gsonable;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class GroupsCache extends JsonFile implements Gsonable<GroupsCache, String>, Listener {

    private final List<GroupEntry> Entries;

    public GroupsCache(File file) {
        super(file);
        Entries = exists() ? load(getClass()).Entries() : new ArrayList<>();
    }

    public void save() {
        save(this);
    }

    @Override
    public String id() {
        return "Groups";
    }

    @Override
    public String name() {
        return "Groups";
    }

    @Override
    public Class<? extends GroupsCache> type() {
        return GroupsCache.class;
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onCreate(GroupLoadEvent e) {
        if (e.newGroup()) Entries.add(e.group().Entry());
        else {
            GroupEntry ours = Entries().stream().filter(entry -> entry.id().equals(e.group().id())).findFirst().orElse(null);
            if (ours != null) e.group().Entry(ours);
        }
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onRemove(GroupUnloadEvent e) {
        if (e.deleted()) Entries.removeIf(entry -> entry.id().equals(e.group().id()));
    }

    public List<GroupEntry> Entries() {
        return Entries;
    }

    public void exists(String name, Call<Boolean> result) {
        Optional<GroupEntry> found = Entries.parallelStream().filter(groupEntry -> groupEntry.name().equalsIgnoreCase(name)).findFirst();
        result.call(found.isPresent());
    }
}
